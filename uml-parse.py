#!/usr/bin/env python3

from uml.lexer import lexer
from uml.parser import parser
from os.path import isfile


def vis_name(c):
    if c == '+':
        return 'public'
    elif c == '-':
        return 'private'
    elif c == '#':
        return 'protected'
    else:
        raise ValueError('Unknown Visibility ' + repr(c))


def def_val(type_name):
    if type_name == 'void' or type_name == '':
        return ''
    elif type_name == 'boolean':
        return 'false'
    elif type_name in ['int', 'char', 'short', 'byte', 'long']:
        return '0'
    elif type_name == 'float':
        return '0.0f'
    elif type_name == 'double':
        return '0.0'
    else:
        return 'null'


def arg_list(args):
    args = [a + ' arg' + str(i+1) for i, a in enumerate(args)]
    return ', '.join(args)


def write_class(t, fout):
    t = t.lower()
    if t == 'abstract':
        fout.write('abstract class ')
    elif t == 'class':
        fout.write('class ')
    elif t == 'interface':
        fout.write('interface ')


def write_field(field, fout):
    vis, name, vtype = field
    fout.write('\t{} {} {};\n'.format(vis_name(vis), vtype, name))


def write_constructor(constructor, fout):
    cvis, cname, cargs = constructor
    fout.write('\t{} {}({}) {{\n'.format(
        vis_name(cvis), cname, arg_list(cargs)))
    fout.write('\t}\n\n')


def write_method(method, fout):
    vis, name, args, rtn = method
    fout.write('\t{} {} {}({}) {{\n'.format(
        vis_name(vis), rtn, name, arg_list(args)))
    rtn_val = def_val(rtn)
    if rtn_val:
        fout.write('\t\treturn {};\n'.format(rtn_val))
    fout.write('\t}\n\n')


def load_file(file):
    with open(file, 'r') as fin:
        return fin.read()


def main(infile, outfile):
    lines = load_file(infile)
    # lexer.input(lines)
    # for tok in iter(lexer.token, None):
    #     print(tok)

    t, n, f, c, m = parser.parse(lines)

    with open(outfile, 'w') as fout:
        write_class(t, fout)
        fout.write(n + ' {\n')

        for field in f:
            write_field(field, fout)
        fout.write('\n')

        write_constructor(c, fout)
        for method in m:
            write_method(method, fout)

        fout.write('}\n')


def make_name(name):
    try:
        last_dot = name.rindex('.')
        new_name = name[:last_dot]
    except ValueError:
        new_name = name
    new_name += '.java'
    return new_name


if __name__ == "__main__":
    import argparse

    ap = argparse.ArgumentParser(
        description='Take a UML text file and generate a java source file from it')
    ap.add_argument('input', help='the input file')
    ap.add_argument(
        '-o', '--output', help='specify the output file, if not specified the filename will be the same as input')

    args = ap.parse_args()
    out = args.output or make_name(args.input)
    main(args.input, out)
