from setuptools import setup


def readme():
    with open('README.md', 'r') as f:
        return f.read()


setup(name='umlparse',
      version='0.1',
      description='Generate an empty Java class from the UML code',
      long_description=readme(),
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'Intended Audience :: Education',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.7'
      ],
      url='http://gitlab.com/twh2898/uml-parser',
      author='Thomas Harrison',
      author_email='twh2898@vt.edu',
      license='MIT',
      packages=['uml'],
      scripts=['uml-parse.py'],
      install_requires=[
          'ply',
      ],
      include_package_data=True,
      zip_safe=False)
