import ply.lex as lex

# List of token names.   This is always required
tokens = (
    'CTYPE',
    'NAME',
    'HR',
    'VIS',
    'DECORATE',
)

literals = ':,[]()<>?'

# Regular expression rules for simple tokens
t_NAME = r'([a-zA-Z]([a-zA-Z0-9_]*[a-zA-Z0-9])?\.?)+'
t_VIS = r'[-\+\#]'
t_DECORATE = r'[_\*/]'


def t_HR(t):
    r'--'
    return t


def t_CTYPE(t):
    r'<<(Abstract|Class|Interface)>>'
    t.value = t.value[2:-2]
    return t


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


t_ignore = ' \r\t'


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


# Build the lexer
lexer = lex.lex()
