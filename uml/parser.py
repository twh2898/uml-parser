import ply.yacc as yacc
from uml.lexer import tokens

start = 'start'


def p_start(p):
    '''start : CTYPE class_name hr fields hr constructor methods'''
    p[0] = (p[1], p[2], p[4], p[6], p[7])


def p_class_name(p):
    '''class_name : decorator type decorator
    class_name : type'''
    if len(p) > 2:
        if p[1] != p[3]:
            raise SyntaxError('Expected same decorator, got:', p[3])
        p[0] = p[2]
    else:
        p[0] = p[1]


def p_decorator(p):
    '''decorator : DECORATE'''
    p[0] = p[1]


def p_hr(p):
    '''hr : HR '''
    pass


def p_fields(p):
    '''fields : field fields
    fields : field'''
    if len(p) > 2:
        p[0] = (p[1],) + p[2]
    else:
        p[0] = (p[1],)


def p_fields_empty(p):
    '''fields : empty'''
    p[0] = tuple()


def p_field(p):
    '''field : decorator field decorator
    field : visibility NAME ':' type'''
    if len(p) > 4:
        p[0] = (p[1], p[2], p[4])
    else:
        if p[1] != p[3]:
            raise SyntaxError('Expected same decorator, got:', p[3])
        p[0] = p[2]


def p_visibility(p):
    '''visibility : VIS'''
    p[0] = p[1]


def p_type(p):
    '''type : NAME '[' ']'
    type : NAME '<' typelist '>' 
    type : NAME'''
    if len(p) > 2 and p[2] == '[':
        p[0] = p[1] + '[]'
    elif len(p) > 2 and p[2] == '<':
        p[0] = p[1] + '<' + p[3] + '>'
    else:
        p[0] = p[1]


def p_typelist(p):
    '''typelist : type ',' typelist
    typelist : type typelist
    typelist : '?' typelist
    typelist : type'''
    if len(p) > 3:
        p[0] = p[1] + ', ' + p[3]
    elif len(p) > 2:
        p[0] = p[1] + ' ' + p[2]
    else:
        p[0] = p[1]


def p_constructor(p):
    '''constructor : decorator constructor decorator
    constructor : visibility NAME '(' params ')' '''
    if len(p) > 4:
        p[0] = (p[1], p[2], p[4])
    else:
        p[0] = p[2]


def p_methods(p):
    '''methods : method methods
    methods : method'''
    if len(p) > 2:
        p[0] = (p[1],) + p[2]
    else:
        p[0] = (p[1],)


def p_methods_empty(p):
    '''methods : empty'''
    p[0] = tuple()


def p_method(p):
    '''method : decorator method decorator
    method : visibility NAME '(' params ')' ':' type'''
    if len(p) > 4:
        p[0] = (p[1], p[2], p[4], p[7])
    else:
        if p[1] != p[3]:
            raise SyntaxError('Expected same decorator, got:', p[3])
        p[0] = p[2]


def p_params(p):
    '''params : param ',' params
    params : param'''
    if len(p) > 2:
        p[0] = (p[1],) + p[3]
    else:
        p[0] = (p[1],)


def p_params_empty(p):
    '''params : empty'''
    p[0] = tuple()


def p_param(p):
    '''param : type'''
    p[0] = p[1]


def p_empty(p):
    '''empty :'''
    pass


def p_error(p):
    print("Syntax error on line:", p.lineno)
    print((p))


# Build the parser
parser = yacc.yacc(debug=False, write_tables=False)
