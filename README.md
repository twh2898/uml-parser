# UML Parser

UML Parser is a python3 implemented parser for UML language. This parser will generate an empty Java class from the UML code.

## Usage

```
$ python uml-parse.py -h
usage: uml-parse.py [-h] [-o OUTPUT] input

Take a UML text file and generate a java source file from it

positional arguments:
  input                 the input file

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        specify the output file, if not specified the filename
                        will be the same as input
```

## Examples

Example UML text files and their generated Java files are in the [examples](examples) folder.

## Licence

uml parser uses the [MIT](LICENSE) license
