abstract class Heap {
	private int storeSpace;

	public Heap(byte[] arg1, int arg2, int arg3) {
	}

	private void shiftUp(int arg1) {
	}

	private void shiftDown(int arg1) {
	}

	private boolean isLeaf(int arg1) {
		return false;
	}

	private int parent(int arg1) {
		return 0;
	}

	private int leftChild(int arg1) {
		return 0;
	}

	public boolean isEmpty() {
		return false;
	}

	public boolean isFull() {
		return false;
	}

	public int availableRecordSpace() {
		return 0;
	}

	public int storedRecords() {
		return 0;
	}

	public void buildHeap() {
	}

	public void insert(Buffer arg1, int arg2) {
	}

	public void storeFirst() {
	}

	public void removeFirst() {
	}

	public boolean isBefore(int arg1, float arg2, int arg3, float arg4) {
		return false;
	}

}
