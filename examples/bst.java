class BST<T extends Comparable<? extends T>> {
	private Node root;

	public BST() {
	}

	private Node insert(T arg1, Node arg2) {
		return null;
	}

	private Node findNode(T arg1, Node arg2) {
		return null;
	}

	private Node findFirst(Node arg1) {
		return null;
	}

	private Node remove(T arg1, Node arg2) {
		return null;
	}

	private void iterLeft(Stack<Node> arg1, Node arg2) {
	}

	public boolean isEmpty() {
		return false;
	}

	public int size() {
		return 0;
	}

	public boolean contains(T arg1) {
		return false;
	}

	public void insert(T arg1) {
	}

	public void remove(T arg1) {
	}

	public void clear() {
	}

	public String dump() {
		return null;
	}

	public String toString() {
		return null;
	}

	public Iterator<T> iterator() {
		return null;
	}

}
